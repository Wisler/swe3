using NUnit.Framework;
using ORM.Mapper.BusinessLogic;
using ORM.Mapper.Metadata;
using ORM.TestApp;
using System.Configuration;

namespace ORM.Tests
{
    public class SimpleClassTests
    {
        private TestDbHelper dbHelper = new TestDbHelper();
        [SetUp]
        public void Setup()
        {
            dbHelper.resetDB();
        }

        [TearDown]
        public void TearDown()
        {
            dbHelper.resetDB();
        }

        [Test]
        public void VerifyThatMyDatabaseConnectionStringExists()
        {
            Assert.IsNotNull(ConfigurationManager.ConnectionStrings["MariaDB"]);
        }

        [Test]
        public void CreateEntity_NotNull()
        {
            MetaDataManager manager = new MetaDataManager();
            Entity<Person> e = new Entity<Person>(manager);

            Assert.That(e != null);
        }

        [Test]
        public void CreateEntity_HasFields()
        {
            MetaDataManager manager = new MetaDataManager();
            Entity<Person> e = new Entity<Person>(manager);

            Assert.AreEqual(2, manager.metaDataEntities[typeof(Person)].Fields.Length);
        }

        [Test]
        public void CreateEntityWithPrimaryKey_HasPrimaryKey()
        {
            MetaDataManager manager = new MetaDataManager();
            Entity<Person> e = new Entity<Person>(manager);

            Assert.AreEqual(1, manager.metaDataEntities[typeof(Person)].PrimaryKeys.Length);
        }

        [Test]
        public void CreateEntity_SavesInDb_OK()
        {
            MetaDataManager manager = new MetaDataManager();
            Entity<Person> e = new Entity<Person>(manager);

            Assert.AreEqual(1, dbHelper.GetTables().Length);
        }

        [Test]
        public void SaveObjectWithIdInDb_OK()
        {
            MetaDataManager manager = new MetaDataManager();
            Entity<Person> e = new Entity<Person>(manager);
            Person p = new Person()
            {
                Id = 1,
                Name = "Stefan"
            };

            e.Insert(p);

            Assert.AreEqual(1, dbHelper.GetTables().Length);
            var p_new = e.Select(1);
            Assert.AreEqual(p.Id, p_new.Id);
            Assert.AreEqual(p.Name, p_new.Name);
        }

        [Test]
        public void UpdateObjectInDb_Ok()
        {
            MetaDataManager manager = new MetaDataManager();
            Entity<Person> e = new Entity<Person>(manager);
            Person p = new Person()
            {
                Id = 1,
                Name = "Stefan"
            };

            e.Insert(p);

            Assert.AreEqual(1, dbHelper.GetTables().Length);
            var p_new = e.Select(1);
            Assert.AreEqual(p.Id, p_new.Id);
            Assert.AreEqual(p.Name, p_new.Name);

            p.Name = "Klaus";
            e.Update(p);

            Assert.AreEqual(1, dbHelper.GetTables().Length);
            var p_new1 = e.Select(1);
            Assert.AreEqual(p.Id, p_new1.Id);
            Assert.AreEqual(p.Name, p_new1.Name);
        }

        [Test]
        public void DeleteObjectInDb_Ok()
        {
            MetaDataManager manager = new MetaDataManager();
            Entity<Person> e = new Entity<Person>(manager);
            Person p = new Person()
            {
                Id = 1,
                Name = "Stefan"
            };

            e.Insert(p);

            Assert.AreEqual(1, dbHelper.GetTables().Length);
            var p_new = e.Select(1);
            Assert.AreEqual(p.Id, p_new.Id);
            Assert.AreEqual(p.Name, p_new.Name);

            e.Delete(p);

            Assert.AreEqual(1, dbHelper.GetTables().Length);
            var p_new1 = e.Select(1);
            Assert.IsNull(p_new1);
        }
    }
}