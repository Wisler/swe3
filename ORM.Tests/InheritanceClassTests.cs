using NUnit.Framework;
using ORM.Mapper.BusinessLogic;
using ORM.Mapper.Metadata;
using ORM.TestApp;
using System.Configuration;

namespace ORM.Tests
{
    public class InheritanceClassTests
    {
        private TestDbHelper dbHelper = new TestDbHelper();
        [SetUp]
        public void Setup()
        {
            dbHelper.resetDB();
        }

        [TearDown]
        public void TearDown()
        {
            dbHelper.resetDB();
        }

        [Test]
        public void VerifyThatMyDatabaseConnectionStringExists()
        {
            Assert.IsNotNull(ConfigurationManager.ConnectionStrings["MariaDB"]);
        }

        [Test]
        public void CreateEntity_NotNull()
        {
            MetaDataManager manager = new MetaDataManager();
            Entity<Teacher> e = new Entity<Teacher>(manager);

            Assert.That(e != null);
        }

        [Test]
        public void CreateEntity_HasFields()
        {
            MetaDataManager manager = new MetaDataManager();
            Entity<Teacher> e = new Entity<Teacher>(manager);

            Assert.AreEqual(1, manager.metaDataEntities[typeof(Teacher)].Fields.Length);
        }

        [Test]
        public void CreateEntityWithPrimaryKey_HasPrimaryKey()
        {
            MetaDataManager manager = new MetaDataManager();
            Entity<Teacher> e = new Entity<Teacher>(manager);

            Assert.AreEqual(0, manager.metaDataEntities[typeof(Teacher)].PrimaryKeys.Length);
        }

        [Test]
        public void CreateEntity_SavesInDb_OK()
        {
            MetaDataManager manager = new MetaDataManager();
            Entity<Teacher> e = new Entity<Teacher>(manager);

            Assert.AreEqual(2, dbHelper.GetTables().Length);
        }

        [Test]
        public void SaveObjectWithIdInDb_OK()
        {
            MetaDataManager manager = new MetaDataManager();
            Entity<Teacher> e = new Entity<Teacher>(manager);
            Teacher t = new Teacher()
            {
                Id = 1,
                Name = "Stefan",
                Title = "BSc"
            };

            e.Insert(t);

            Assert.AreEqual(2, dbHelper.GetTables().Length);
            var t_new = e.Select(1);
            Assert.AreEqual(t.Id, t_new.Id);
            Assert.AreEqual(t.Name, t_new.Name);
            Assert.AreEqual(t.Title, t_new.Title);
        }

        [Test]
        public void UpdateObjectInDb_Ok()
        {
            MetaDataManager manager = new MetaDataManager();
            Entity<Teacher> e = new Entity<Teacher>(manager);
            Teacher t = new Teacher()
            {
                Id = 1,
                Name = "Stefan",
                Title = "none"
            };

            e.Insert(t);

            Assert.AreEqual(2, dbHelper.GetTables().Length);
            var t_new = e.Select(1);
            Assert.AreEqual(t.Id, t_new.Id);
            Assert.AreEqual(t.Name, t_new.Name);
            Assert.AreEqual(t.Title, t_new.Title);

            t.Name = "Klaus";
            t.Title = "BSc";
            e.Update(t);

            Assert.AreEqual(2, dbHelper.GetTables().Length);
            var t_new1 = e.Select(1);
            Assert.AreEqual(t.Id, t_new1.Id);
            Assert.AreEqual(t.Name, t_new1.Name);
            Assert.AreEqual(t.Title, t_new1.Title);
        }

        [Test]
        public void DeleteObjectInDb_Ok()
        {
            MetaDataManager manager = new MetaDataManager();
            Entity<Teacher> e = new Entity<Teacher>(manager);
            Teacher t = new Teacher()
            {
                Id = 1,
                Name = "Stefan",
                Title = "BSc"
            };

            e.Insert(t);

            Assert.AreEqual(2, dbHelper.GetTables().Length);
            var t_new = e.Select(1);
            Assert.AreEqual(t.Id, t_new.Id);
            Assert.AreEqual(t.Name, t_new.Name);
            Assert.AreEqual(t.Title, t_new.Title);

            e.Delete(t);

            Assert.AreEqual(2, dbHelper.GetTables().Length);
            var t_new1 = e.Select(1);
            Assert.IsNull(t_new1);
        }
    }
}