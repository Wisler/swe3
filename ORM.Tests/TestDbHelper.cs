﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace ORM.Tests
{
    public class TestDbHelper
    {
        private MySqlConnection connection;

        public TestDbHelper()
        {
            string connectionString = "SERVER=localhost; DATABASE=swe3test; UID=swe3; PASSWORD=swe3;";
            connection = new MySqlConnection(connectionString);
        }

        public void resetDB()
        {
            this.DropTables(GetTables());
        }

        public String[] GetTables()
        {
            string getTablesStmt = "SELECT table_name FROM information_schema.tables WHERE table_type = 'base table' AND table_schema='swe3test';";

            List<string> list = new List<string>();
            if (OpenConnection() == true)
            {
                using (MySqlCommand cmd = new MySqlCommand(getTablesStmt, connection))
                {
                    MySqlDataReader dataReader = cmd.ExecuteReader();
                    while (dataReader.Read())
                    {
                        list.Add(dataReader["table_name"].ToString());
                    }
                    dataReader.Close();
                }
                CloseConnection();
            }
            return list.ToArray();
        }

        private void DropTables(String[] tables)
        {
            if (OpenConnection() == true)
            {
                using (MySqlCommand cmd = new MySqlCommand("SET FOREIGN_KEY_CHECKS = 0;", connection))
                {
                    cmd.ExecuteNonQuery();
                }
                foreach (string table in tables)
                {
                    using (MySqlCommand cmd = new MySqlCommand(String.Format("drop table {0}", table), connection))
                    {
                        cmd.ExecuteNonQuery();
                    }
                }
                using (MySqlCommand cmd = new MySqlCommand("SET FOREIGN_KEY_CHECKS = 0;", connection))
                {
                    cmd.ExecuteNonQuery();
                }
                CloseConnection();
            }
        }

        #region connection handling
        /// <summary>
        /// Tries to open the connection to the DB
        /// </summary>
        /// <returns>true: if successfully opened connectiont to DB, false otherwise</returns>
        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        Console.WriteLine("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        Console.WriteLine("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }
        /// <summary>
        /// Tries to close the connection to the DB
        /// </summary>
        /// <returns>true: if successfully closed connectiont to DB, false otherwise</returns>
        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }
        #endregion
    }
}
