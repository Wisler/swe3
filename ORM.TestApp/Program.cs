﻿using MySql.Data.MySqlClient;
using ORM.Mapper;
using System;

namespace ORM.TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new MyContext();

            #region testing simple class
            var person1 = new Person
            {
                Id = 1,
                Name = "Stefan"
            };
            var person2 = new Person
            {
                Name = "Klaus"
            };
            var person3 = new Person
            {
                Name = "Markus"
            };

            context.Persons.Insert(person1);
            context.Persons.Insert(person2);
            context.Persons.Insert(person3);

            person3.Name = "Andreas";

            context.Persons.Update(person3);

            context.Persons.Delete(person2);
            #endregion

            #region testing derived class
            var teacher1 = new Teacher
            {
                Name = "Hannes",
                Title = "BSc"
            };
            var teacher2 = new Teacher
            {
                Name = "Fabian",
                Title = "BSc"
            };

            context.Teachers.Insert(teacher1);
            context.Teachers.Insert(teacher2);

            teacher2.Name = "Lukas";
            teacher2.Title = "MSc";
            context.Teachers.Update(teacher2);

            context.Teachers.Delete(teacher1);
            #endregion

            Console.WriteLine("Fin");
        }
    }
}


// attribute for tablename (siehe bsp von lv)
// add check for insert if id already in db
// check if it works for multiple primary keys
// it can only ever be one autoincrement
// string primary key?