﻿using ORM.Mapper.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.TestApp
{
    public class MyContext : DbContext
    {
        public MyContext()
        {
            DbReset();
            Persons = new Entity<Person>(_metaDataManager);
            Teachers = new Entity<Teacher>(_metaDataManager);
        }
        public Entity<Person> Persons { get; set; }
        public Entity<Teacher> Teachers { get; set; }
    }
}
