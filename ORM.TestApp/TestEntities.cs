﻿using ORM.Mapper.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.TestApp
{
    public class Person
    {
        [PrimaryKey]
        public int Id { get; set; }

        [MaxLength(64)]
        public string Name { get; set; }
    }

    public class Teacher : Person
    {
        [MaxLength(64)]
        public string Title { get; set; }
    }
}
