﻿using ORM.Mapper.DataAccess;
using ORM.Mapper.SqlCommands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ORM.Mapper.Metadata
{
    /// <summary>
    /// MetaData manager
    /// </summary>
    public class MetaDataManager
    {
        /// <summary>
        /// List of entities
        /// </summary>
        public Dictionary<Type, EntityInfo> metaDataEntities = new Dictionary<Type, EntityInfo>();

        /// <summary>
        /// Saves a new entity
        /// </summary>
        /// <param name="type">type of entity</param>
        public void Register(Type type)
        {
            if (metaDataEntities.ContainsKey(type))
                return;

            EntityInfo entity = new EntityInfo(type);

            if (entity.HasBaseType)
            {
                this.Register(entity.BaseType);
                entity.AddBaseTypeInformation(metaDataEntities[entity.BaseType]);
            }

            try
            {
                metaDataEntities.Add(type, entity);

                SqlCommandBuilder sqlCommand = new SqlCommandBuilder();
                QueryExecutor executor = new QueryExecutor();
                executor.ExecuteNonQueryCreateTable(sqlCommand.GetCreateTableCommand(entity));
            }
            catch(ArgumentException)
            {
                throw;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public EntityInfo GetEntity(Type type)
        {
            if (metaDataEntities.ContainsKey(type))
                return metaDataEntities[type];
            else
                throw new KeyNotFoundException("This type is not yet mapped");
        }
    }
}
