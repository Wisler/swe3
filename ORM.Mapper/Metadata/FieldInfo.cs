﻿using ORM.Mapper.Helper;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace ORM.Mapper.Metadata
{
    /// <summary>
    /// Class for information about a field
    /// </summary>
    public class FieldInfo
    {
        #region constructors
        /// <summary>
        /// Creates a new instance of this class
        /// </summary>
        /// <param name="propertyInfo">property information</param>
        public FieldInfo(PropertyInfo propertyInfo)
        {
            Name = propertyInfo.Name;
            Type = propertyInfo.PropertyType;
            DbColumnName = Name;
            SetDbDataType();

            foreach (var customAttribute in propertyInfo.CustomAttributes)
            {
                if (customAttribute.AttributeType.Name == "PrimaryKeyAttribute")
                    Constraints.IsPrimaryKey = true;
                if (Constraints.IsPrimaryKey == true && Type == typeof(int))
                    Constraints.IsAutoIncrement = true;
                // max length only for varchar
                if (customAttribute.AttributeType.Name == "MaxLengthAttribute" && DbColumnType == "varchar")
                {
                    int maxLength = (int)customAttribute.ConstructorArguments[0].Value;
                    DbColumnType += $"({maxLength})";
                }
            }

            if (DbColumnType == "varchar")
                DbColumnType = "varchar(255)";
        }
        #endregion

        #region public properties
        /// <summary>
        /// Name of the property
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Type of the property
        /// </summary>
        public Type Type { get; private set; }
        /// <summary>
        /// Name of the DB column for this property
        /// </summary>
        public string DbColumnName { get; private set; }
        /// <summary>
        /// Type of the property in the DB
        /// </summary>
        public string DbColumnType { get; private set; } 
        /// <summary>
        /// Constraints of the property
        /// </summary>
        public Constraints Constraints { get; private set; } = new Constraints();
        #endregion

        #region private methods
        /// <summary>
        /// Sets the DB type of the property
        /// </summary>
        private void SetDbDataType()
        {
            if(!Converter.GetDbType(this.Type, out var dbColumnType))
            {
                //ToDo: Custom Exception
                throw new InvalidOperationException("no such datatype supported yet");
            }
            DbColumnType = dbColumnType;
        }
        #endregion
    }
}
