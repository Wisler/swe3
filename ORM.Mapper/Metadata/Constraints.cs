﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Mapper.Metadata
{
    /// <summary>
    /// Class for constraints
    /// </summary>
    public class Constraints
    {
        /// <summary>
        /// Primary Key constraint
        /// </summary>
        public bool IsPrimaryKey { get; set; } = false;
        public bool IsAutoIncrement { get; set; } = false;
        public bool isForeignKey { get { return !string.IsNullOrEmpty(ForeignKeyReferences); } }
        public string ForeignKeyReferences { get; set; } = null;
    }
}
