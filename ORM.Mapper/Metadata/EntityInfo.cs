﻿using ORM.Mapper.Attributes;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace ORM.Mapper.Metadata
{
    /// <summary>
    /// Class for information about an entity
    /// </summary>
    public class EntityInfo
    {
        #region constructors
        /// <summary>
        /// Creates a new instance of this class
        /// </summary>
        /// <param name="type">Type</param>
        public EntityInfo(Type type)
        {
            List<FieldInfo> fields = new List<FieldInfo>();

            this.Type = type;
            ClassName = type.Name;
            _baseType = type.BaseType;

            PropertyInfo[] propertyInfos = type.GetProperties();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                if (!propertyInfo.CanRead)
                    // ToDo: throw Custom Exception
                    Console.WriteLine("property cannot be read");
                if (!propertyInfo.CanWrite)
                    // ToDo: throw Custom Exception
                    Console.WriteLine("property cannot be written");

                if (propertyInfo.DeclaringType == this.Type)
                    fields.Add(new FieldInfo(propertyInfo));
            }

            this.Fields = fields.ToArray();
        }
        #endregion

        #region private properties
        private Type _baseType { get; set; }

        private List<Tuple<FieldInfo, EntityInfo>> _basePrimaryKeys { get; set; } = new List<Tuple<FieldInfo, EntityInfo>>();
        #endregion

        #region public properties
        /// <summary>
        /// Gets the entity type
        /// </summary>
        public Type Type { get; private set; }

        /// <summary>
        /// Gets the class name
        /// </summary>
        public string ClassName { get; private set; }

        /// <summary>
        /// Gets the table name
        /// </summary>
        //ToDo: custom table name for class (necessary?)
        public string TableName { get { return ClassName; } }

        public bool HasBaseType { get { return BaseType != null; } }

        public Type BaseType
        {
            get
            {
                if (_baseType == typeof(object))
                    return null;
                else
                    return _baseType;
            }
        }

        /// <summary>
        /// Gets the entity fields
        /// </summary>
        public FieldInfo[] Fields { get; private set; }

        /// <summary>
        /// Gets the primary keys
        /// </summary>
        public FieldInfo[] PrimaryKeys
        {
            get
            {
                List<FieldInfo> rval = new List<FieldInfo>();
                foreach (FieldInfo i in Fields)
                {
                    if (i.Constraints.IsPrimaryKey == true) { rval.Add(i); }
                }

                return rval.ToArray();
            }
        }
        /// <summary>
        /// Gets the primary keys
        /// </summary>
        public Tuple<FieldInfo, EntityInfo>[] BasePrimaryKeys
        {
            get
            {
                if (HasBaseType)
                    return _basePrimaryKeys.ToArray();
                else
                    return new Tuple<FieldInfo, EntityInfo>[] { };
            }
        }

        public EntityInfo BaseEntity { get; set; }
        #endregion

        #region public methods
        public void AddBaseTypeInformation(EntityInfo baseEntity)
        {
            BaseEntity = baseEntity;

            foreach(var pk in baseEntity.BasePrimaryKeys)
            {
                _basePrimaryKeys.Add(pk);
            }
            foreach (FieldInfo pk in baseEntity.PrimaryKeys)
            {
                _basePrimaryKeys.Add(new Tuple<FieldInfo, EntityInfo>(pk, baseEntity));
            }
        }
        #endregion
    }
}
