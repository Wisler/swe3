﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Mapper.SqlCommands
{
    /// <summary>
    /// Base class for building SQL commands
    /// </summary>
    internal class SqlCommandBuilderBase
    {
        /// <summary>
        /// SQL command
        /// </summary>
        protected StringBuilder _sqlCommandBuilder = new StringBuilder();

        /// <summary>
        /// Gets the SQL command as a string
        /// </summary>
        /// <returns>SQL command</returns>
        public string GetSqlCommand()
        {
            return _sqlCommandBuilder.ToString();
        }
    }
}
