﻿using ORM.Mapper.Metadata;
using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Mapper.SqlCommands
{
    internal class SqlSingleUpdateCommandBuilder : SqlCommandBuilderBase
    {
        public SqlSingleUpdateCommandBuilder(EntityInfo entity)
        {
            _sqlCommandBuilder.Append($"UPDATE {entity.TableName} SET ");
            AppendColumnsToUpdate(entity);
            _sqlCommandBuilder.Append(" WHERE ");
            AppendWhereStatement(entity);
            _sqlCommandBuilder.Append(";");
        }

        private void AppendColumnsToUpdate(EntityInfo entity)
        {
            string prefix = "";
            foreach (var field in entity.Fields)
            {
                if(field.Constraints.IsPrimaryKey == false)
                {
                    _sqlCommandBuilder.Append(prefix);
                    prefix = ", ";
                    _sqlCommandBuilder.Append(field.DbColumnName);
                    _sqlCommandBuilder.Append("=@");
                    _sqlCommandBuilder.Append(field.DbColumnName);
                }
            }
        }

        private void AppendWhereStatement(EntityInfo entity)
        {
            string prefix = "";
            foreach (var field in entity.BasePrimaryKeys)
            {
                _sqlCommandBuilder.Append(prefix);
                prefix = " AND ";
                _sqlCommandBuilder.Append($"{field.Item2.TableName}_{field.Item1.DbColumnName}");
                _sqlCommandBuilder.Append("=@");
                _sqlCommandBuilder.Append($"{field.Item2.TableName}_{field.Item1.DbColumnName}");
            }
            foreach (var field in entity.PrimaryKeys)
            {
                _sqlCommandBuilder.Append(prefix);
                prefix = " AND ";
                _sqlCommandBuilder.Append(field.DbColumnName);
                _sqlCommandBuilder.Append("=@");
                _sqlCommandBuilder.Append(field.DbColumnName);
            }
        }
    }
}
