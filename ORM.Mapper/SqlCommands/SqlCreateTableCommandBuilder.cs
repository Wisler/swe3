﻿using ORM.Mapper.Metadata;
using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Mapper.SqlCommands
{
    /// <summary>
    /// Class for creating a SQL command for creating a table for an entity
    /// </summary>
    internal class SqlCreateTableCommandBuilder : SqlCommandBuilderBase
    {
        /// <summary>
        /// Creates a new instance of this class
        /// builds a SQL command
        /// </summary>
        /// <param name="entity">Entity</param>
        public SqlCreateTableCommandBuilder(EntityInfo entity)
        {
            AppendTableExistenceCheck(entity.ClassName);

            string prefix = "";
            foreach (var field in entity.BasePrimaryKeys)
            {
                _sqlCommandBuilder.Append(prefix);
                prefix = ", ";
                AppendColumn(field);
            }
            foreach (var field in entity.Fields)
            {
                _sqlCommandBuilder.Append(prefix);
                prefix = ", ";
                AppendColumn(field);
            }

            _sqlCommandBuilder.Append(prefix);
            AppendConstraints(entity);

            _sqlCommandBuilder.Append(");");
        }

        private void AppendTableExistenceCheck(string tablename)
        {
            _sqlCommandBuilder.Append($"CREATE TABLE IF NOT EXISTS {tablename} (");
        }

        private void AppendColumn(FieldInfo field)
        {
            _sqlCommandBuilder.Append($"{field.DbColumnName} {field.DbColumnType}");

            if (field.Constraints.IsAutoIncrement)
                _sqlCommandBuilder.Append(" AUTO_INCREMENT");
        }

        private void AppendColumn(Tuple<FieldInfo, EntityInfo> field)
        {
            _sqlCommandBuilder.Append($"{field.Item2.TableName}_{field.Item1.DbColumnName} {field.Item1.DbColumnType}");

            if (field.Item1.Constraints.IsAutoIncrement)
                _sqlCommandBuilder.Append(" AUTO_INCREMENT");
        }

        private void AppendConstraints(EntityInfo entity)
        {
            AppendPrimaryKeys(entity);
            if (entity.HasBaseType)
                AppendParentEntityPksAsForeignKey(entity);
        }

        private void AppendPrimaryKeys(EntityInfo entity)
        {
            _sqlCommandBuilder.Append("PRIMARY KEY (");

            string prefix = "";
            foreach (var field in entity.BasePrimaryKeys)
            {
                _sqlCommandBuilder.Append(prefix);
                prefix = ", ";
                _sqlCommandBuilder.Append($"{field.Item2.TableName}_{field.Item1.DbColumnName}");
            }
            foreach (FieldInfo field in entity.PrimaryKeys)
            {
                _sqlCommandBuilder.Append(prefix);
                prefix = ", ";
                _sqlCommandBuilder.Append(field.DbColumnName);
            }

            _sqlCommandBuilder.Append(")");
        }

        private void AppendParentEntityPksAsForeignKey(EntityInfo entity)
        {
            foreach(var field in entity.BasePrimaryKeys)
            {
                _sqlCommandBuilder.Append(", ");
                _sqlCommandBuilder.Append($"CONSTRAINT fk_{field.Item2.TableName}_{field.Item1.DbColumnName} FOREIGN KEY ({field.Item2.TableName}_{field.Item1.DbColumnName}) REFERENCES {field.Item2.TableName}({field.Item1.DbColumnName})");
            }
        }
    }
}
