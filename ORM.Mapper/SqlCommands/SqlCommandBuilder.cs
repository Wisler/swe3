﻿using ORM.Mapper.Metadata;
using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Mapper.SqlCommands
{
    /// <summary>
    /// Class to get SQL commands
    /// </summary>
    internal class SqlCommandBuilder
    {
        /// <summary>
        /// Gets the SQL command to create a table for an entity
        /// </summary>
        /// <param name="entity">Entity for which to create a table</param>
        /// <returns>SQL command for creating a table for given entity</returns>
        public string GetCreateTableCommand(EntityInfo entity)
        {
            var commandBuilder = new SqlCreateTableCommandBuilder(entity);
            return commandBuilder.GetSqlCommand();
        }

        public string GetInsertStatementCommand(EntityInfo entity)
        {
            var commandBuilder = new SqlSingleInsertCommandBuilder(entity);
            return commandBuilder.GetSqlCommand();
        }

        public string GetUpdateStatementCommand(EntityInfo entity)
        {
            var commandBuilder = new SqlSingleUpdateCommandBuilder(entity);
            return commandBuilder.GetSqlCommand();
        }

        public string GetDeleteStatementCommand(EntityInfo entity)
        {
            var commandBuilder = new SqlSingleDeleteCommandBuilder(entity);
            return commandBuilder.GetSqlCommand();
        }

        public string GetSelectStatementCommand(EntityInfo entity)
        {
            var commandBuilder = new SqlSingleSelectCommandBuilder(entity);
            return commandBuilder.GetSqlCommand();
        }
    }
}
