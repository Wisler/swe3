﻿using ORM.Mapper.Metadata;
using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Mapper.SqlCommands
{
    internal class SqlSingleSelectCommandBuilder : SqlCommandBuilderBase
    {
        public SqlSingleSelectCommandBuilder(EntityInfo entity)
        {
            _sqlCommandBuilder.Append($"SELECT ");
            AppendColumnsToSelect(entity);
            AppendFromStatement(entity);
            _sqlCommandBuilder.Append(" WHERE ");
            AppendWhereStatement(entity);
            _sqlCommandBuilder.Append(";");
        }

        private void AppendColumnsToSelect(EntityInfo entity)
        {
            string prefix = "";
            if (entity.HasBaseType)
            {
                AppendColumnsToSelect(entity.BaseEntity);
                prefix = ", ";
            }
            foreach (var field in entity.Fields)
            {
                _sqlCommandBuilder.Append(prefix);
                prefix = ", ";
                _sqlCommandBuilder.Append($"{entity.TableName}.{field.DbColumnName}");
            }
        }

        private void AppendFromStatement(EntityInfo entity)
        {
            _sqlCommandBuilder.Append($" FROM {entity.TableName}");
            foreach(var field in entity.BasePrimaryKeys)
            {
                _sqlCommandBuilder.Append($" INNER JOIN {field.Item2.TableName} ");
                _sqlCommandBuilder.Append($"ON {entity.TableName}.{field.Item2.TableName}_{field.Item1.DbColumnName} = ");
                _sqlCommandBuilder.Append($"{field.Item2.TableName}.{field.Item1.DbColumnName}");
            }
        }

        private void AppendWhereStatement(EntityInfo entity)
        {
            string prefix = "";
            if (entity.HasBaseType)
            {
                AppendWhereStatement(entity.BaseEntity);
                prefix = ", ";
            }
            foreach (var pks in entity.PrimaryKeys)
            {
                _sqlCommandBuilder.Append(prefix);
                prefix = ", ";
                _sqlCommandBuilder.Append($"{entity.TableName}.{pks.DbColumnName}");
                _sqlCommandBuilder.Append("=@");
                _sqlCommandBuilder.Append($"{entity.TableName}.{pks.DbColumnName}");
            }
        }
    }
}
