﻿using ORM.Mapper.Metadata;
using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Mapper.SqlCommands
{
    /// <summary>
    /// Class for creating a SQL command for inserting an entry
    /// </summary>
    internal class SqlSingleInsertCommandBuilder : SqlCommandBuilderBase
    {
        /// <summary>
        /// Creates a new instance of this class
        /// builds a SQL command
        /// </summary>
        /// <param name="entity">Entity</param>
        public SqlSingleInsertCommandBuilder(EntityInfo entity)
        {
            _sqlCommandBuilder.Append($"INSERT INTO {entity.TableName} (");
            AppendColumnsToInsert(entity);
            _sqlCommandBuilder.Append(") VALUES (");
            AppendValuesPlaceHolderToInsert(entity);
            _sqlCommandBuilder.Append(");");
        }

        private void AppendColumnsToInsert(EntityInfo entity)
        {
            string prefix = "";
            foreach (var field in entity.BasePrimaryKeys)
            {
                _sqlCommandBuilder.Append(prefix);
                prefix = ", ";
                _sqlCommandBuilder.Append($"{field.Item2.TableName}_{field.Item1.DbColumnName}");
            }
            foreach (FieldInfo field in entity.Fields)
            {
                if (field.Constraints.IsAutoIncrement == false)
                {
                    _sqlCommandBuilder.Append(prefix);
                    prefix = ", ";
                    _sqlCommandBuilder.Append(field.DbColumnName);
                }
            }
        }

        private void AppendValuesPlaceHolderToInsert(EntityInfo entity)
        {
            string prefix = "";
            foreach (var field in entity.BasePrimaryKeys)
            {
                    _sqlCommandBuilder.Append(prefix);
                    prefix = ", ";
                    _sqlCommandBuilder.Append($"@{field.Item2.TableName}_{field.Item1.DbColumnName}");
            }
            foreach (FieldInfo field in entity.Fields)
            {
                if (field.Constraints.IsAutoIncrement == false)
                {
                    _sqlCommandBuilder.Append(prefix);
                    prefix = ", ";
                    _sqlCommandBuilder.Append($"@{field.DbColumnName}");
                }
            }
        }
    }
}
