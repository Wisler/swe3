﻿namespace ORM.Mapper.Attributes
{
    /// <summary>
    /// contains all supported attributes
    /// </summary>
    public enum AttributeTypes
    {
        PrimaryKey,
        ForeignKey,
        NotNull,
        MaxLength,
        ColumnName,
        TableName,
    }
}

