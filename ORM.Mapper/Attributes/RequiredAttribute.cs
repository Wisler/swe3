﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Mapper.Attributes
{
    /// <summary>
    /// Attribute for required fields
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class RequiredAttribute : Attribute
    {
    }
}
