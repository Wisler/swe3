﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Mapper.Attributes
{
    /// <summary>
    /// Attribute for foreign key
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    class ForeignKeyAttribute : Attribute
    {
    }
}
