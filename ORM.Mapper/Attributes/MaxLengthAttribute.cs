﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Mapper.Attributes
{
    /// <summary>
    /// Attribute for max length (of strings)
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class MaxLengthAttribute : Attribute
    {
        /// <summary>
        /// max length
        /// </summary>
        private int _maxLength;

        /// <summary>
        /// Creates a new instance of this class
        /// </summary>
        /// <param name="maxLength">max length</param>
        public MaxLengthAttribute(int maxLength)
        {
            _maxLength = maxLength;
        }
    }
}
