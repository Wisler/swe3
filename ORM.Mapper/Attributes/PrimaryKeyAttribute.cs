﻿using System;

namespace ORM.Mapper.Attributes
{
    /// <summary>
    /// Attribute for primary key
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class PrimaryKeyAttribute : Attribute
    {
    }
}

