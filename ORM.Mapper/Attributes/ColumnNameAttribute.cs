﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Mapper.Attributes
{
    /// <summary>
    /// Attribute for custom column name
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ColumnNameAttribute : Attribute
    {
        /// <summary>
        /// custom column name
        /// </summary>
        private string _columnName;

        /// <summary>
        /// Creates a new instance of this class
        /// </summary>
        /// <param name="columnName">custom column name</param>
        public ColumnNameAttribute(string columnName)
        {
            _columnName = columnName;
        }
    }
}
