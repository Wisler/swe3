﻿using ORM.Mapper.DataAccess;
using ORM.Mapper.Metadata;
using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Mapper.BusinessLogic
{
    /// <summary>
    /// DB Context Base
    /// </summary>
    public class DbContext
    {
        protected readonly MetaDataManager _metaDataManager = new MetaDataManager();

        public void DbReset()
        {
            QueryExecutor executor = new QueryExecutor();
            executor.resetDB();
        }
    }
}
