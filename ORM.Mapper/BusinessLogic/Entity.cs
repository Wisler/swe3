﻿using ORM.Mapper.Metadata;
using System;
using System.Collections.Generic;
using ORM.Mapper.SqlCommands;
using System.Text;
using ORM.Mapper.DataAccess;
using System.Linq;

namespace ORM.Mapper.BusinessLogic
{
    /// <summary>
    /// Entity class
    /// </summary>
    /// <typeparam name="T">Type of entity</typeparam>
    public class Entity<T> where T: class, new()
    {
        /// <summary>
        /// MetaData manager
        /// </summary>
        public MetaDataManager _metaDataManager;

        /// <summary>
        /// Creates a new instance of this class
        /// </summary>
        /// <param name="metaDataManager">MetaData manager</param>
        public Entity(MetaDataManager metaDataManager)
        {
            _metaDataManager = metaDataManager;
            _metaDataManager.Register(typeof(T));
        }

        public T Insert(T obj)
        {
            EntityInfo entity = _metaDataManager.GetEntity(obj.GetType());

            Insert(obj, entity);

            return obj;
        }

        private void Insert(object obj, EntityInfo entity)
        {
            if(entity.HasBaseType)
            {
                Insert(obj, entity.BaseEntity);
            }

            SqlCommandBuilder sqlCommand = new SqlCommandBuilder();
            QueryExecutor executor = new QueryExecutor();
            long id = executor.ExecuteSkalarSingleInsert(sqlCommand.GetInsertStatementCommand(entity), obj, entity);
            
            FieldInfo autoPk = entity.PrimaryKeys.SingleOrDefault(f => f.Constraints.IsAutoIncrement == true);
            if(autoPk != null)
                typeof(T).GetProperty(autoPk.Name).SetValue(obj, (int)id);
        }

        public T Update(T obj)
        {
            EntityInfo entity = _metaDataManager.GetEntity(obj.GetType());

            Update(obj, entity);

            return obj;
        }

        private void Update(T obj, EntityInfo entity)
        {
            if(entity.HasBaseType)
            {
                Update(obj, entity.BaseEntity);
            }

            SqlCommandBuilder sqlCommand = new SqlCommandBuilder();
            QueryExecutor executor = new QueryExecutor();
            executor.ExecuteNonQuerySingleUpdate(sqlCommand.GetUpdateStatementCommand(entity), obj, entity);
        }

        public void Delete(T obj)
        {
            EntityInfo entity = _metaDataManager.GetEntity(obj.GetType());

            Delete(obj, entity);
        }

        private void Delete(T obj, EntityInfo entity)
        {
            SqlCommandBuilder sqlCommand = new SqlCommandBuilder();
            QueryExecutor executor = new QueryExecutor();
            executor.ExecuteNonQuerySingleUpdate(sqlCommand.GetDeleteStatementCommand(entity), obj, entity);

            if(entity.HasBaseType)
            {
                Delete(obj, entity.BaseEntity);
            }
        }

        public T Select(params object[] pks)
        {
            object rval = null;
            EntityInfo entity = _metaDataManager.GetEntity(typeof(T));

            SqlCommandBuilder sqlCommand = new SqlCommandBuilder();
            QueryExecutor executor = new QueryExecutor();
            rval = executor.ExecuteQuerySingleSelect(sqlCommand.GetSelectStatementCommand(entity), entity, pks);

            return (T)rval;
        }
    }
}

