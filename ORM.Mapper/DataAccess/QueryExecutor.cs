﻿using MySql.Data.MySqlClient;
using ORM.Mapper.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ORM.Mapper.DataAccess
{
    /// <summary>
    /// Class for executing SQL commands
    /// </summary>
    internal class QueryExecutor
    {
        /// <summary>
        /// Creates a new instance of this class
        /// </summary>
        public QueryExecutor()
        {
            connection = DbMariaDbAccess.GetConnection();
        }

        /// <summary>
        /// connection to DB
        /// </summary>
        private readonly MySqlConnection connection;

        #region execute
        /// <summary>
        /// Executes a create table statement
        /// </summary>
        /// <param name="sqlCommand">the statement to execute</param>
        public void ExecuteNonQueryCreateTable(string sqlCommand)
        {
            if (OpenConnection() == true)
            {
                using (var command = new MySqlCommand(sqlCommand, connection))
                {
                    command.ExecuteNonQuery();
                }

                CloseConnection();
            }
        }

        /// <summary>
        /// Executes a create table statement
        /// </summary>
        /// <param name="sqlCommand">the statement to execute</param>
        public long ExecuteSkalarSingleInsert(string sqlCommand, object obj, EntityInfo entity)
        {
            long id = -1;
            if (OpenConnection() == true)
            {
                using (var command = new MySqlCommand(sqlCommand, connection))
                {
                    foreach (var field in entity.BasePrimaryKeys)
                    {
                        var v = obj.GetType().GetProperty(field.Item1.Name).GetValue(obj);

                        command.Parameters.AddWithValue($"@{field.Item2.TableName}_{field.Item1.DbColumnName}", v);
                    }
                    foreach (FieldInfo field in entity.Fields)
                    {
                        var v = obj.GetType().GetProperty(field.Name).GetValue(obj);

                        command.Parameters.AddWithValue($"@{field.DbColumnName}", v);
                    }

                    command.ExecuteNonQuery();
                    id = command.LastInsertedId;
                }

                CloseConnection();
            }
            return id;
        }

        public void ExecuteNonQuerySingleUpdate(string sqlCommand, object obj, EntityInfo entity)
        {
            if (OpenConnection() == true)
            {
                using (var command = new MySqlCommand(sqlCommand, connection))
                {
                    foreach (var field in entity.BasePrimaryKeys)
                    {
                        var v = obj.GetType().GetProperty(field.Item1.Name).GetValue(obj);

                        command.Parameters.AddWithValue($"{field.Item2.TableName}_{field.Item1.DbColumnName}", v);
                    }
                    foreach (FieldInfo field in entity.Fields)
                    {
                        var v = obj.GetType().GetProperty(field.Name).GetValue(obj);

                        command.Parameters.AddWithValue($"@{field.DbColumnName}", v);
                    }
                    command.ExecuteNonQuery();
                }

                CloseConnection();
            }
        }

        public void ExecuteNonQuerySingleDelete(string sqlcommand, object obj, EntityInfo entity)
        {
            if (OpenConnection() == true)
            {
                using (var command = new MySqlCommand(sqlcommand, connection))
                {
                    foreach (var field in entity.BasePrimaryKeys)
                    {
                        var v = obj.GetType().GetProperty(field.Item1.Name).GetValue(obj);

                        command.Parameters.AddWithValue($"{field.Item2.TableName}_{field.Item1.DbColumnName}", v);
                    }
                    foreach (FieldInfo field in entity.Fields)
                    {
                        var v = obj.GetType().GetProperty(field.Name).GetValue(obj);

                        command.Parameters.AddWithValue($"@{field.DbColumnName}", v);
                    }
                    command.ExecuteNonQuery();
                }

                CloseConnection();
            }
        }

        public object ExecuteQuerySingleSelect(string sqlcommand, EntityInfo entity, IEnumerable<object> pks)
        {
            object rval = null;
            if (OpenConnection() == true)
            {
                using (var command = new MySqlCommand(sqlcommand, connection))
                {
                    int i = 0;
                    foreach(var pk in entity.BasePrimaryKeys)
                    {
                        var v = pks.ElementAt(i);
                        i++;

                        command.Parameters.AddWithValue($"@{pk.Item2.TableName}.{pk.Item1.DbColumnName}", v);
                    }
                    foreach (FieldInfo pk in entity.PrimaryKeys)
                    {
                        var v = pks.ElementAt(i);
                        i++;

                        command.Parameters.AddWithValue($"@{entity.TableName}.{pk.DbColumnName}", v);
                    }

                    MySqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.Read())
                    {
                        rval = Activator.CreateInstance(entity.Type);

                        var temp = entity;
                        while (temp.HasBaseType)
                        {
                            temp = temp.BaseEntity;

                            foreach (var field in temp.Fields)
                            {
                                entity.Type.GetProperty(field.Name).SetValue(rval, dataReader[$"{field.Name}"]);
                            }
                        } 

                        foreach (var field in entity.Fields)
                        {
                            entity.Type.GetProperty(field.Name).SetValue(rval, dataReader[$"{field.Name}"]);
                        }
                    }
                    else
                    {
                        //ToDo throw exception
                    }
                    dataReader.Close();
                }

                CloseConnection();
            }
            return rval;
        }
        #endregion

        #region connection handling
        /// <summary>
        /// Tries to open the connection to the DB
        /// </summary>
        /// <returns>true: if successfully opened connectiont to DB, false otherwise</returns>
        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        Console.WriteLine("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        Console.WriteLine("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }
        /// <summary>
        /// Tries to close the connection to the DB
        /// </summary>
        /// <returns>true: if successfully closed connectiont to DB, false otherwise</returns>
        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }
        #endregion

        //ToDo: remove once project is finished
        #region DbReset
        public void resetDB()
        {
            this.DropTables(GetTables());
        }

        public String[] GetTables()
        {
            string getTablesStmt = "SELECT table_name FROM information_schema.tables WHERE table_type = 'base table' AND table_schema='swe3';";

            List<string> list = new List<string>();
            if (OpenConnection() == true)
            {
                using (MySqlCommand cmd = new MySqlCommand(getTablesStmt, connection))
                {
                    MySqlDataReader dataReader = cmd.ExecuteReader();
                    while (dataReader.Read())
                    {
                        list.Add(dataReader["table_name"].ToString());
                    }
                    dataReader.Close();
                }
                CloseConnection();
            }
            return list.ToArray();
        }

        private void DropTables(String[] tables)
        {
            if (OpenConnection() == true)
            {
                foreach (string table in tables)
                {
                    using (MySqlCommand cmd = new MySqlCommand(String.Format("drop table {0}", table), connection))
                    {
                        cmd.ExecuteNonQuery();
                    }
                }
                CloseConnection();
            }
        }
        #endregion
    }
}
