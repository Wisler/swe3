﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace ORM.Mapper.DataAccess
{
    /// <summary>
    /// Class for DB connection
    /// </summary>
    internal class DbMariaDbAccess
    {
        /// <summary>
        /// Gets the connector for the local MariaDB
        /// </summary>
        /// <returns>MySqlConnection</returns>
        public static MySqlConnection GetConnection()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MariaDB"].ConnectionString;
            return new MySqlConnection(connectionString);
        }
    }
}
