﻿using ORM.Mapper.Metadata;
using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Mapper.Helper
{
    /// <summary>
    /// Class for custom converter
    /// </summary>
    internal static class Converter
    {
        /// <summary>
        /// Contains supported conversions of types
        /// </summary>
        private static IDictionary<Type, string> _dataTypesDict = new Dictionary<Type, string>
        {
            {typeof(int), "int" },
            {typeof(string), "varchar" }
        };

        /// <summary>
        /// Gets the corresponding DB type of an OO type
        /// </summary>
        /// <param name="type">OO type</param>
        /// <param name="dbType">DB type</param>
        /// <returns>DB type</returns>
        public static bool GetDbType(Type type, out string dbType)
        {
            return _dataTypesDict.TryGetValue(type, out dbType);
        }

        public static object ToFieldType(FieldInfo fieldinfo, object value)
        {
            if (fieldinfo.Type == typeof(bool))
            {
                if (value is int) { return ((int)value != 0); }
                if (value is short) { return ((short)value != 0); }
                if (value is long) { return ((long)value != 0); }
            }

            if (fieldinfo.Type == typeof(short)) { return Convert.ToInt16(value); }
            if (fieldinfo.Type == typeof(int)) { return Convert.ToInt32(value); }
            if (fieldinfo.Type == typeof(long)) { return Convert.ToInt64(value); }

            //if (fieldinfo.Type.IsEnum) { return Enum.ToObject(fieldinfo.Type, value); }

            return value;
        }
    }
}
